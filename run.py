
from flask import Flask
from flask import jsonify
import socket



app = Flask(__name__)

containerid = socket.gethostname()



@app.route('/hello/<name>')
def api_article(name):
    #Test variables
    #Check hostname is set
    if 'containerid' not in globals():
        return jsonify('error, unable to detect containerid')
        #Check hostname is not empty
        return(containerid)
    elif containerid == "":
          return jsonify('error, unable to detect containerid')
    #Check name variable is set
    elif 'name' not in locals():
        return jsonify('error, unable to detect your name')
    msg = '{' + 'message: Hello {} from {}'.format(name,containerid) + '}'
    return jsonify(msg)




if __name__ == "__main__":

    app.run(debug=True, host='0.0.0.0', port = 5001)

